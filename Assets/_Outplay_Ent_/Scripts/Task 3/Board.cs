﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Task3
{
    public class Board : MonoBehaviour
    {
        [SerializeField] int width = 10;
        [SerializeField] int height = 7;

        [SerializeField] List<Jewel> jewels = null;
        BoardMap map;

        private void Awake()
        {
            map = new BoardMap(width, height);

            InstantiateJewels();
        }

        private void InstantiateJewels()
        {
            for (int x = 0; x < map.Width; x++)
            {
                for (int y = 0; y < map.Height; y++)
                {
                    JewelKind kind = map.GetJewelKind(x, y);

                    Jewel j = jewels.FirstOrDefault(i => i.Kind == kind);

                    if (j) Instantiate(j, new Vector3(x, y, 0), Quaternion.identity, transform);
                }
            }
        }

        public Move CalculateBestMoveForBoard()
        {
            Move result = new Move(-1, -1, MoveDirection.Down);

            var allMoves = map.GetAllValidMoves();

            if (allMoves.Count > 0)
            {
                var allMoveResults = map.GetAllMoveStats(allMoves);

                result = allMoveResults.Aggregate((i, j) => i.MatchAmount > j.MatchAmount ? i : j).move;
            }

            return result;
        }
    }
}
