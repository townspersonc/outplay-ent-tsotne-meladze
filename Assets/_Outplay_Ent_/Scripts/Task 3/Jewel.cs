﻿using UnityEngine;

namespace Task3
{
    public class Jewel : MonoBehaviour
    {
        [SerializeField] JewelKind kind = JewelKind.Blue;
        public JewelKind Kind => kind;
    }
}
