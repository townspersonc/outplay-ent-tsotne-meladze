﻿using System.Collections.Generic;
using UnityEngine;

namespace Task3
{
    struct MoveStatisticData
    {
        public Move move;
        public int MatchAmount => matchCoordinates.Count;
        public List<Vector2Int> matchCoordinates;

        public MoveStatisticData(Move move, List<Vector2Int> matchCoordinates)
        {
            this.move = move;
            this.matchCoordinates = matchCoordinates;
        }
    }
}
