﻿using UnityEngine;

namespace Task3
{
    public struct Move
    {
        public int x;
        public int y;
        public MoveDirection direction;
        public bool HorizontalDirection => direction == MoveDirection.Left || direction == MoveDirection.Right;

        public Move(int x, int y, MoveDirection dir)
        {
            this.x = x;
            this.y = y;
            this.direction = dir;
        }

        public Vector2Int ActiveCoordinate => new Vector2Int(x, y);

        public Vector2Int PassiveCoordinate
        {
            get
            {
                if (direction == MoveDirection.Left)
                {
                    return new Vector2Int(x - 1, y);
                }
                else if (direction == MoveDirection.Right)
                {
                    return new Vector2Int(x + 1, y);
                }
                else if (direction == MoveDirection.Down)
                {
                    return new Vector2Int(x, y - 1);
                }
                else
                {
                    //up
                    return new Vector2Int(x, y + 1);
                }
            }
        }
    }
}
