﻿namespace Task3
{
    public enum MoveDirection
    {
        Up,
        Down,
        Left,
        Right
    }
}
