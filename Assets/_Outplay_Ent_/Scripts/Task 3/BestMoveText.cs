﻿using UnityEngine;
using UnityEngine.UI;

namespace Task3
{
    public class BestMoveText : MonoBehaviour
    {
        [SerializeField] Text text = null;
        [SerializeField] Board board = null;

        private void Start()
        {
            Move bestMove = board.CalculateBestMoveForBoard();

            text.text = $"Best Move is ({bestMove.x}:{bestMove.y}) with Direction {bestMove.direction.ToString()}";
        }
    }
}
