﻿using Extentions;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Task3
{
    class BoardMap
    {
        List<List<JewelKind>> jewels;

        public int Width => jewels.Count;
        public int Height => jewels[0].Count;

        public BoardMap(int width, int height)
        {
            InitialiseMapList(width, height);
        }

        void InitialiseMapList(int width, int height)
        {
            jewels = new List<List<JewelKind>>();

            for (int x = 0; x < width; x++)
            {
                jewels.Add(new List<JewelKind>(new JewelKind[height]));
            }

            PopulateMap();
        }

        private void PopulateMap()
        {
            List<JewelKind> allKinds = new List<JewelKind>();
            allKinds.Add(JewelKind.Blue);
            allKinds.Add(JewelKind.Green);
            allKinds.Add(JewelKind.Indigo);
            allKinds.Add(JewelKind.Orange);
            allKinds.Add(JewelKind.Red);
            allKinds.Add(JewelKind.Violet);
            allKinds.Add(JewelKind.Yellow);

            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    List<JewelKind> curJewels = new List<JewelKind>(allKinds);
                    //Making sure no 3 or more matching jewels are spawned horizontally or vertically.
                    if (GetJewelKind(x - 1, y) == GetJewelKind(x - 2, y)) curJewels.Remove(GetJewelKind(x - 1, y));
                    if (GetJewelKind(x, y - 1) == GetJewelKind(x, y - 2)) curJewels.Remove(GetJewelKind(x, y - 1));

                    jewels[x][y] = curJewels.RandomMember();
                }
            }
        }

        public JewelKind GetJewelKind(int x, int y)
        {
            if (CoordinatesAreValid(x, y)) return jewels[x][y];

            return JewelKind.Empty;
        }

        public JewelKind GetJewelKind(Vector2Int coordinate)
        {
            return GetJewelKind(coordinate.x, coordinate.y);
        }

        public bool CoordinatesAreValid(int x, int y)
        {
            return BonusMath.Between(x, 0, Width) && BonusMath.Between(y, 0, Height);
        }

        public bool MoveIsValid(Move m)
        {
            if (m.x == 0 && m.direction == MoveDirection.Left) return false;
            if (m.x == Width - 1 && m.direction == MoveDirection.Right) return false;
            if (m.y == 0 && m.direction == MoveDirection.Down) return false;
            if (m.y == Height - 1 && m.direction == MoveDirection.Up) return false;

            return CoordinatesAreValid(m.x, m.y);
        }

        bool DoMove(Move m)
        {
            if (!MoveIsValid(m)) return false;

            SwitchJewelKinds(m);

            return true;
        }

        private void SwitchJewelKinds(Move m)
        {
            Vector2Int passiveC = m.PassiveCoordinate;
            JewelKind active = GetJewelKind(m.x, m.y);

            jewels[m.x][m.y] = GetJewelKind(passiveC);
            jewels[passiveC.x][passiveC.y] = active;
        }

        public List<MoveStatisticData> GetAllMoveStats(List<Move> allMoves)
        {
            List<MoveStatisticData> res = new List<MoveStatisticData>();

            foreach (Move i in allMoves)
            {
                res.Add(GetMoveResult(i));
            }

            return res;
        }

        public MoveStatisticData GetMoveResult(Move m)
        {
            MoveStatisticData res;

            var active = m.ActiveCoordinate;
            var passive = m.PassiveCoordinate;

            HashSet<Vector2Int> coordinates = new HashSet<Vector2Int>();

            if (DoMove(m))
            {
                coordinates.UnionWith(HorizontalCheck(active, GetJewelKind(active)));
                coordinates.UnionWith(VerticalCheck(active, GetJewelKind(active)));

                coordinates.UnionWith(HorizontalCheck(passive, GetJewelKind(passive)));
                coordinates.UnionWith(VerticalCheck(passive, GetJewelKind(passive)));

                //to Undo the move.
                DoMove(m);
            }

            res = new MoveStatisticData(m, coordinates.ToList());

            return res;
        }

        List<Vector2Int> VerticalCheck(Vector2Int start, JewelKind forJewel)
        {
            List<Vector2Int> result = new List<Vector2Int>();
            result.Add(start);

            for (int y = start.y + 1; y < Height; y++)
            {
                if (GetJewelKind(start.x, y) != forJewel) break;

                result.Add(new Vector2Int(start.x, y));
            }

            for (int y = start.y - 1; y >= 0; y--)
            {
                if (GetJewelKind(start.x, y) != forJewel) break;

                result.Add(new Vector2Int(start.x, y));
            }

            return result.Count > 2 ? result : new List<Vector2Int>();
        }

        List<Vector2Int> HorizontalCheck(Vector2Int start, JewelKind forJewel)
        {
            List<Vector2Int> result = new List<Vector2Int>();
            result.Add(start);

            for (int x = start.x + 1; x < Width; x++)
            {
                if (GetJewelKind(x, start.y) != forJewel) break;

                result.Add(new Vector2Int(x, start.y));
            }

            for (int x = start.x - 1; x >= 0; x--)
            {
                if (GetJewelKind(x, start.y) != forJewel) break;

                result.Add(new Vector2Int(x, start.y));
            }

            //Match is considered match of 3 or more jewels
            return result.Count > 2 ? result : new List<Vector2Int>();
        }

        public List<Move> GetAllValidMoves()
        {
            List<Move> result = new List<Move>();

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (GetJewelKind(x, y) == JewelKind.Empty) continue;

                    if (y != Height - 1) result.Add(new Move(x, y, MoveDirection.Up));
                    if (y != 0) result.Add(new Move(x, y, MoveDirection.Down));
                    if (x != 0) result.Add(new Move(x, y, MoveDirection.Left));
                    if (x != Width - 1) result.Add(new Move(x, y, MoveDirection.Right));
                }
            }

            return result;
        }
    }
}
