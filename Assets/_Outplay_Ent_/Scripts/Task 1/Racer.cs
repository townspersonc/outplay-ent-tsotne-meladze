﻿namespace Task1
{
    /// <summary>
    /// Dummy class to make errors go away.
    /// </summary>
    public class Racer
    {
        public bool IsAlive() => true;
        public void Update(float timeDelta) { }
        public bool IsCollidable() => true;
        public bool CollidesWith(Racer r) => false;


        public void Destroy() { }
    }
}
