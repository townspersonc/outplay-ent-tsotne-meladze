﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Task1
{
    public class RacersController : MonoBehaviour
    {
        const float timeDeltaMultiplier = 1000f;

        void UpdateRacers(float deltaTimeS, List<Racer> racers)
        {
            //Set new positions for all racers;
            UpdateRacerPositions(racers, deltaTimeS);

            //Get all collided racer ids.
            List<int> ids = CollidedRacerIDs(racers);

            //Destroy-Explode collided racers and leave only non-colided racers in list(Because of ref keyword).
            DestroyRacers(ref racers, ids);
        }

        /// <summary>
        /// Explodes and Destroys all racers at IDs.
        /// </summary>
        /// <param name="racers"></param>
        /// <param name="IDs"></param>
        private void DestroyRacers(ref List<Racer> racers, List<int> IDs)
        {
            for (int i = IDs.Count - 1; i >= 0; i--)
            {
                int id = IDs[i];
                Racer r = racers[id];

                OnRacerExplodes(r);
                r.Destroy();
                racers.RemoveAt(id);
            }
        }

        void UpdateRacerPositions(List<Racer> racers, float timeDeltaS)
        {
            foreach (Racer r in racers)
            {
                if (r.IsAlive())
                {
                    r.Update(timeDeltaS * timeDeltaMultiplier);
                }
            }
        }

        /// <summary>
        /// Detects collisions between all Racers.
        /// </summary>
        /// <param name="racers"></param>
        /// <returns> List of integers containing collided racer IDs in passed list.</returns>
        List<int> CollidedRacerIDs(List<Racer> racers)
        {
            HashSet<int> collidedRacers = new HashSet<int>();

            for (int i = 0; i < racers.Count; i++)
            {
                Racer first = racers[i];
                if (!first.IsCollidable()) continue;

                //Starting second racers from
                for (int j = i + 1; j < racers.Count; j++)
                {
                    Racer second = racers[j];
                    if (!second.IsCollidable()) continue;

                    if (first.CollidesWith(second))
                    {
                        collidedRacers.Add(i);
                        collidedRacers.Add(j);
                    }
                }
            }

            return collidedRacers.ToList();
        }

        public void OnRacerExplodes(Racer r) { }
    }
}
