﻿using UnityEngine;

namespace Extentions
{
    /// <summary>
    /// Some bonus methods that are useful.
    /// </summary>
    public static class BonusMath
    {
        /// <summary>
        /// Usefull for getting percentagewise values for 2 different ranges. Example 50 from 1 to 100 is 5 from 1 to 10.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="in_min">Current range min</param>
        /// <param name="in_max">Current range max</param>
        /// <param name="out_min">Target range min</param>
        /// <param name="out_max">Target range max</param>
        /// <returns></returns>
        public static float Map(float x, float in_min, float in_max, float out_min, float out_max)
        {
            return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        }

        /// <summary>
        /// Gets how close value is from min to max, if x == min than 0, if x== max than 1.
        /// </summary>
        /// <param name="x">value to be calculated</param>
        /// <param name="min">minimum value</param>
        /// <param name="max">maximum value</param>
        /// <returns></returns>
        public static float FromZeroToOne(float x, float min, float max)
        {
            x = Mathf.Clamp(x, min, max);

            return (x - min) / (max - min);
        }

        /// <summary>
        /// Reverse of FromZeroToOne.
        /// </summary>
        /// <param name="fromZeroToOne"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static float GetValueFromPercentage(float fromZeroToOne, float min, float max)
        {
            return min + (max - min) * fromZeroToOne;
        }

        /// <summary>
        /// Checks if val is between between1 and between1.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="between1">Order of these does not matter</param>
        /// <param name="between2">Order of these does not matter</param>
        /// <returns></returns>
        public static bool Between(float val, float between1, float between2)
        {
            if (between1 > between2)
            {
                Swap(ref between2, ref between1);
            }

            return val >= between1 && val <= between2;
        }

        /// <summary>
        /// Checks if val is between between1 and between1.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="between1">Order of these does not matter</param>
        /// <param name="between2">Order of these does not matter</param>
        /// <returns></returns>
        public static bool Between(int val, int between1, int between2)
        {
            if (between1 > between2)
            {
                Swap(ref between2, ref between1);
            }

            return val >= between1 && val <= between2;
        }

        /// <summary>
        /// Checks if val is outside outside1 and outside1.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="outside1">Order of these does not matter</param>
        /// <param name="outside2">Order of these does not matter</param>
        /// <returns></returns>
        public static bool Outside(float val, float outside1, float outside2)
        {
            if (outside1 > outside2)
            {
                Swap(ref outside2, ref outside1);
            }

            return val < outside1 || val > outside2;
        }

        /// <summary>
        /// Checks if val is outside outside1 and outside1.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="outside1">Order of these does not matter</param>
        /// <param name="outside2">Order of these does not matter</param>
        /// <returns></returns>
        public static bool Outside(float val, int outside1, int outside2)
        {
            if (outside1 > outside2)
            {
                Swap(ref outside2, ref outside1);
            }

            return val < outside1 && val > outside2;
        }

        /// <summary>
        /// Swaps values of a and b.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public static void Swap(ref float a, ref float b)
        {
            a = a + b;
            b = a - b;
            a = a - b;
        }

        /// <summary>
        /// Swaps values of a and b.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public static void Swap(ref int a, ref int b)
        {
            a = a + b;
            b = a - b;
            a = a - b;
        }

        /// <summary>
        /// Returns Modulo as defined in Math.
        /// </summary>
        /// <param name="devidant"></param>
        /// <param name="devisor"></param>
        /// <returns></returns>
        public static int RealModulo(int devidant, int devisor)
        {
            int a = devidant % devisor;
            return a < 0 ? a + devisor : a;
        }

        public static float RealModulo(float devidant, float devisor)
        {
            float a = devidant % devisor;
            return a < 0 ? a + devisor : a;
        }

        /// <summary>
        /// If a ball was to travel some distance uninterupted on a plane, how would that distance translate to position between walls if same force was applied. A.K.A Transforming normal throw distance to Ping Pong like bounce distance between 2 walls with specified width.
        /// </summary>
        /// <param name="distance"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public static float MapDistanceBetweenWallsInverted(float distance, float width)
        {
            int quotient = (int)(distance / width);
            float reminder = BonusMath.RealModulo(distance, width);

            if (distance < 0)
            {
                //Even Quotient
                if ((quotient & 1) == 0)
                {
                    return width - reminder;
                }
                else
                {
                    if (reminder == 0)
                    {
                        return width;
                    }

                    return reminder;
                }
            }
            else
            {
                //Even Quotient
                if ((quotient & 1) == 0)
                {
                    return reminder;
                }
                else
                {
                    return width - reminder;
                }
            }
        }
    }
}
