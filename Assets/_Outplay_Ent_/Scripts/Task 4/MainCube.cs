﻿using Extentions;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Task4
{
    public class MainCube : MonoBehaviour
    {
        [SerializeField] float moveSpeed = 10f;
        [SerializeField] float rotationSpeed = 10f;
        [SerializeField] ObjectSpawner targetSpawner = null;
        [SerializeField] LayerMask obstacleLayer = default;
        List<Transform> targetTransforms;
        CubeTargetContainer activeTarget;

        public UnityEvent WinEvent;
        public UnityEvent LoseEvent;

        private void Start()
        {
            targetTransforms = targetSpawner.GetObjectTransforms;

            activeTarget = new CubeTargetContainer(targetTransforms[0], 0);
        }

        private void Update()
        {
            HandleMovement();
            HandleRotation();
        }

        private void UpdateActiveTarget()
        {
            int newId = activeTarget.ID + 1;

            //All targets have been reached.
            if (newId == targetTransforms.Count)
            {
                LastTargetHasBeenReached();
            }
            else
            {
                Transform nextTrans = targetTransforms[newId];

                activeTarget = new CubeTargetContainer(nextTrans, newId);
            }
        }

        private void LastTargetHasBeenReached()
        {
            WinEvent.Invoke();
            DestroyCube();
        }

        void DestroyCube()
        {
            Destroy(gameObject);
        }

        private void HandleRotation()
        {
            var direction = (activeTarget.Position - transform.position).normalized;
            var targetQuat = Quaternion.LookRotation(direction);

            transform.rotation = Quaternion.Lerp(transform.rotation, targetQuat, rotationSpeed * Time.deltaTime);
        }

        private void HandleMovement()
        {
            transform.position = Vector3.MoveTowards(transform.position, activeTarget.Position, moveSpeed * Time.deltaTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.GetInstanceID() == activeTarget.TransInstanceID)
            {
                UpdateActiveTarget();
            }
            else if (obstacleLayer.Contains(other.gameObject.layer))
            {
                LoseEvent.Invoke();
                DestroyCube();
            }

        }

        class CubeTargetContainer
        {
            Transform transform;
            public Vector3 Position => transform.position;
            public int TransInstanceID => transform.GetInstanceID();

            int listID;
            public int ID => listID;

            public CubeTargetContainer(Transform t, int id)
            {
                transform = t;
                listID = id;
            }
        }
    }
}
