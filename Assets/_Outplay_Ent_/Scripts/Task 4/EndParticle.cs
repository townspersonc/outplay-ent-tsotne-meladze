﻿using UnityEngine;

namespace Task4
{

    public class EndParticle : MonoBehaviour
    {
        [SerializeField] MainCube cube = null;

        [SerializeField] GameObject winParticle = null;
        [SerializeField] GameObject lossParticle = null;

        void Start()
        {
            cube.WinEvent.AddListener(PlayWinParticle);
            cube.LoseEvent.AddListener(PlayLossParticle);
        }

        void PlayWinParticle()
        {
            Instantiate(winParticle, transform.position, transform.rotation, transform);
        }

        void PlayLossParticle()
        {
            Instantiate(lossParticle, transform.position, transform.rotation, transform);
        }
    }
}
