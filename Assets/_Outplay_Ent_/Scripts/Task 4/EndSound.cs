﻿using UnityEngine;

namespace Task4
{

    public class EndSound : MonoBehaviour
    {
        [SerializeField] MainCube cube = null;
        [SerializeField] AudioSource source = null;

        [SerializeField] AudioClip winSound = null;
        [SerializeField] AudioClip lossSound = null;

        void Start()
        {
            cube.WinEvent.AddListener(PlayWinSound);
            cube.LoseEvent.AddListener(PlayLossSound);
        }

        void PlayWinSound()
        {
            source.PlayOneShot(winSound);
        }

        void PlayLossSound()
        {
            source.PlayOneShot(lossSound);
        }
    }
}
