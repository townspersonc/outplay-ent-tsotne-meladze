﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    [SerializeField] GameObject objectPrefab = null;
    [SerializeField] int objectAmount = 5;

    [SerializeField] Vector3 spawnRange = new Vector3(10, 10, 10);
    Vector3 RandomSpawnPosition
    {
        get
        {
            Vector3 result;

            result.x = Random.Range(transform.position.x, spawnRange.x);
            result.y = Random.Range(transform.position.y, spawnRange.z);
            result.z = Random.Range(transform.position.z, spawnRange.z);

            return result;
        }
    }

    public List<Transform> GetObjectTransforms
    {
        get
        {
            List<Transform> result = new List<Transform>();

            for (int i = 0; i < transform.childCount; i++)
            {
                result.Add(transform.GetChild(i));
            }

            return result;
        }
    }

    private void Awake()
    {
        if (transform.childCount < objectAmount)
        {
            SpawnWithClear();
        }
    }

    [ContextMenu("Spawn")]
    public void Spawn()
    {
        for (int i = 0; i < objectAmount; i++)
        {
            Instantiate(objectPrefab, RandomSpawnPosition, Quaternion.identity, transform);
        }
    }

    [ContextMenu("Clear")]
    public void Clear()
    {
        while (transform.childCount > 0)
        {
            if (Application.isPlaying)
            {
                Destroy(transform.GetChild(0).gameObject);
            }
            else
            {
                DestroyImmediate(transform.GetChild(0).gameObject);
            }
        }
    }

    [ContextMenu("SpawnWithClear")]
    public void SpawnWithClear()
    {
        Clear();
        Spawn();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.grey;
        Gizmos.DrawWireCube(Vector3.Lerp(transform.position, spawnRange, 0.5f), spawnRange);
    }
}
