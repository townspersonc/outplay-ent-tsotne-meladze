﻿using UnityEngine;

public class TransformFollower : MonoBehaviour
{
    [SerializeField] Transform target = null;
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float rotationSpeed = 2f;

    [SerializeField] bool fixedUpdateType = false;

    private void Update()
    {
        if (!fixedUpdateType)
        {
            HandleMovement();
        }
    }

    private void FixedUpdate()
    {
        if (fixedUpdateType)
        {
            HandleMovement();
        }
    }

    void HandleMovement()
    {
        if (target)
        {
            transform.position = Vector3.Slerp(transform.position, target.position, Time.deltaTime * moveSpeed);
            transform.rotation = Quaternion.Slerp(transform.rotation, target.rotation, Time.deltaTime * rotationSpeed);
        }
    }
}
