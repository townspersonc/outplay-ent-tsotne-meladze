﻿using System;
using System.Collections;
using UnityEngine;

namespace Task2
{
    public class TaskManager : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] EnvironmentSetup environmentSetup = null;
        [SerializeField] Rigidbody2D ball = null;

        [Header("Variables")]
        [SerializeField] Vector2 throwVelocity = Vector2.one * 10f;
        [SerializeField] Vector2 startPos = Vector2.one * 0.5f;
        [SerializeField] [Range(1f, 100f)] float width = 10f;
        [SerializeField] float gravity = -9.81f;
        [SerializeField] float heightAtQuestion = 3f;
        public float Height => heightAtQuestion;

        float timeOfFlight;

        public Action<float> OnNewResultCalculated;

        private void Start()
        {
            ResetBall();
        }

        public void Shoot()
        {
            OnValidate();
            ball.isKinematic = false;
            ball.velocity = throwVelocity;

            StartCoroutine(StopBallAfterTime(timeOfFlight));
        }

        void ResetBall()
        {
            ball.transform.position = startPos;
            ball.isKinematic = true;
            ball.velocity = Vector2.zero;
        }

        private void OnValidate()
        {
            float newResult = -1;
            timeOfFlight = -1;

            Physics2D.gravity = new Vector2(Physics2D.gravity.x, gravity);
            environmentSetup.SetUpFor(width, ball.transform.localScale.x / 2f);
            ResetBall();
            SUVAT.TryCalculateXPositionAtHeight(heightAtQuestion, startPos, throwVelocity, gravity, width, ref newResult, out timeOfFlight);

            OnNewResultCalculated?.Invoke(newResult);
        }

        private IEnumerator StopBallAfterTime(float time)
        {
            if (time <= 0) yield break;

            yield return new WaitForSeconds(time);

            if (!ball.isKinematic)
            {
                ball.velocity = Vector2.zero;
                ball.isKinematic = true;
            }
        }
    }
}
