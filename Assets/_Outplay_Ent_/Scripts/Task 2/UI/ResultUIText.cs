﻿using UnityEngine;
using UnityEngine.UI;

namespace Task2
{
    public class ResultUIText : MonoBehaviour
    {
        [SerializeField] Text valueText = null;
        [SerializeField] TaskManager taskManager = null;

        void OnEnable()
        {
            taskManager.OnNewResultCalculated += UpdateText;
        }
        void OnDisable()
        {
            taskManager.OnNewResultCalculated -= UpdateText;
        }


        void UpdateText(float res)
        {
            valueText.text = $"Result : width = {res.ToString("F2")}";
        }
    }
}
