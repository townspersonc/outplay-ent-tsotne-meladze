﻿using UnityEngine;
using UnityEngine.UI;

namespace Task2
{
    public class BallVelocityUIText : MonoBehaviour
    {
        [SerializeField] Text valueText = null;
        [SerializeField] Rigidbody2D ball = null;

        private void Update()
        {
            valueText.text = $"Velocity ({ball.velocity.x.ToString("F2")} : {ball.velocity.y.ToString("F2")})";
        }
    }
}
