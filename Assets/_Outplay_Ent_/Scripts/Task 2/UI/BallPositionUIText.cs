﻿using UnityEngine;
using UnityEngine.UI;

namespace Task2
{
    public class BallPositionUIText : MonoBehaviour
    {
        [SerializeField] Text valueText = null;
        [SerializeField] Transform ball = null;

        private void Update()
        {
            valueText.text = $"Position ({ball.position.x.ToString("F2")} : {ball.position.y.ToString("F2")})";
        }
    }
}
