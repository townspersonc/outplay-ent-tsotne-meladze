﻿using UnityEngine;

namespace Task2
{
    public class HeightLine : MonoBehaviour
    {
        [SerializeField] TaskManager taskManager = null;

        private void Update()
        {
            transform.position = new Vector3(transform.position.x, taskManager.Height, transform.position.z);
        }
    }
}
