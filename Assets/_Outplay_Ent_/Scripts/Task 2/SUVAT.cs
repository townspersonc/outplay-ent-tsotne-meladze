﻿using Extentions;
using UnityEngine;

namespace Task2
{
    public static class SUVAT
    {
        public static float FinalVelocity(float initialVelocity, float acceleration, float displacement)
        {
            return Mathf.Sqrt((Mathf.Pow(initialVelocity, 2) + (2 * acceleration * displacement)));
        }

        public static float Time(float finalVelocity, float initialVelocity, float acceleration)
        {
            return (finalVelocity - initialVelocity) / acceleration;
        }

        public static float Displacement(float initialVelocity, float finalVelocity, float time)
        {
            return ((initialVelocity + finalVelocity) / 2) * time;
        }

        public static bool TryCalculateXPositionAtHeight(float h, Vector2 p, Vector2 v, float G, float w, ref float xPosition, out float timeOfFlight)
        {
            if (!WillReachPoint(p.y, h, G, v.y))
            {
                xPosition = float.NegativeInfinity;
                timeOfFlight = -1f;
                return false;
            }

            //If we came to this point than there is a solution.

            //Rewriting input to local terms.
            Vector2 initialVelocity = v;
            float acceleration = G;

            float time = 0;
            float directionToTarget = Mathf.Sign(h - p.y);

            if (directionToTarget != Mathf.Sign(initialVelocity.y))
            {
                //There is change in direction of Y movement. Therefore to calculate cumulative Time we will need to calculate time in 2 stages: Accendance, Decendance.
                //Stage 1: Getting further from target until initial velocity runs out.

                float ty1 = TimeTillPeak(initialVelocity.y, acceleration, out float displacementY1);


                //Stage 2: Getting closer to target thanks to accelerational pull.

                //Calulating for positive values saves us from unintended results from suvat formulas.
                float accel = Mathf.Abs(acceleration);
                float displacementY2 = Mathf.Abs(h - (p.y + displacementY1));

                //Cumulative time of flight for Y which matches time of flight on x axis.
                time = ty1 + TimeForOnlyConvergance(0f, accel, displacementY2);
            }
            else
            {
                //No change in direction of Y movement.
                //Calulating for positive values saves us from unintended results from suvat formulas.

                float initVelY = Mathf.Abs(initialVelocity.y);
                float accel = directionToTarget > 0 ? acceleration : -acceleration;
                float displacementY = Mathf.Abs(h - p.y);

                time = TimeForOnlyConvergance(initVelY, accel, displacementY);
            }

            //Time to calculate Displacement X
            float displacementX = initialVelocity.x * time;

            xPosition = BonusMath.MapDistanceBetweenWallsInverted(p.x + displacementX, w);
            timeOfFlight = time;
            return true;
        }

        /// <summary>
        /// This method has meaning only when object is never getting further from target.
        /// </summary>
        /// <param name="initialVelocity"></param>
        /// <param name="acceleration"></param>
        /// <param name="displacement"></param>
        /// <returns></returns>
        public static float TimeForOnlyConvergance(float initialVelocity, float acceleration, float displacement)
        {
            float finalVelocity = FinalVelocity(initialVelocity, acceleration, displacement);
            return Mathf.Abs(Time(finalVelocity, initialVelocity, acceleration));
        }

        public static float TimeTillPeak(float initialVelocity, float acceleration, out float displacement)
        {
            float time = Time(0f, initialVelocity, acceleration);
            displacement = Displacement(initialVelocity, 0f, time);

            return time;
        }

        /// <summary>
        /// This method makes sence only if diplacement and acceleration have different signs.
        /// </summary>
        /// <param name="displacement"></param>
        /// <param name="acceleration"></param>
        /// <returns></returns>
        public static float NeededVelocity(float displacement, float acceleration)
        {
            float neededVelocity = Mathf.Sqrt(-(displacement * 2 * acceleration));

            return Mathf.Sign(acceleration) > 0 ? -neededVelocity : neededVelocity;
        }

        public static bool WillReachPoint(float startPoint, float targetPoint, float acceleration, float initialVelocity)
        {
            float directionToTarget = Mathf.Sign(targetPoint - startPoint);
            float accelerationSign = Mathf.Sign(acceleration);

            if (directionToTarget == accelerationSign)
            {
                //If direction to target matches direction of acceleration(gravity) target will eventually be reached no matter what initial velocity is.
                return true;
            }

            //If acceleration direction as well as velocity direction is against desired direction, there is nothing that can make object reach target.
            float initialVelocitySign = Mathf.Sign(initialVelocity);
            if (initialVelocitySign == accelerationSign) return false;

            float neededVelocity = NeededVelocity(targetPoint - startPoint, acceleration);

            return Mathf.Abs(initialVelocity) > Mathf.Abs(neededVelocity);
        }
    }
}
