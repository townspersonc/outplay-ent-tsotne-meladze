﻿using UnityEngine;

namespace Task2
{
    public class EnvironmentSetup : MonoBehaviour
    {
        [SerializeField] Transform rightWall = null;

        /// <summary>
        /// This method handles positioning Right wall so that environment fits question at hand. 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="ballRadius"></param>
        public void SetUpFor(float width, float ballRadius)
        {
            var rightWallPosition = rightWall.position;

            rightWallPosition.x = width;
            rightWallPosition.x += (rightWall.transform.localScale.x / 2f);//Add half width of wall itself.
            rightWallPosition.x += ballRadius;//Add radius of ball so that we can calculate as if ball was a point.

            rightWall.transform.localPosition = rightWallPosition;
        }
    }
}
