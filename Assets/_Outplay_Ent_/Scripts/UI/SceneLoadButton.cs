﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoadButton : MonoBehaviour
{
    [SerializeField] int sceneID = 0;
    [SerializeField] GameObject selectedVisual = null;
    [SerializeField] Button button = null;

    void Start()
    {
        selectedVisual.SetActive(SceneManager.GetActiveScene().buildIndex == sceneID);
        button.onClick.AddListener(Load);
    }

    public void Load()
    {
        SceneManager.LoadScene(sceneID);
    }
}
